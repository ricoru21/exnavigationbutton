package com.richardoruna.exnavigationbutton;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;


public class DummyFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "color";

    // TODO: Rename and change types of parameters
    private int mColor = R.color.colorD929;
    private FrameLayout frameLayout;

    public DummyFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static DummyFragment newInstance(int param1) {
        DummyFragment fragment = new DummyFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mColor = getArguments().getInt(ARG_PARAM1);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dummy, container, false);
        frameLayout = (FrameLayout) view.findViewById(R.id.fondo);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        frameLayout.setBackgroundColor(getResources().getColor(mColor));
    }
}
