package com.richardoruna.exnavigationbutton;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.aurelhubert.ahbottomnavigation.notification.AHNotification;

public class MainActivity extends AppCompatActivity {

    NoSwipePager viewpager;
    AHBottomNavigation bottomNavigation;
    FrameLayout fragment;

    int[] colors = {R.color.colorFacebook, R.color.colorGoogle,
            R.color.colorD929, R.color.colorPrimaryDark, R.color.colorAccent};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewpager = (NoSwipePager) findViewById(R.id.viewpager);
        fragment = (FrameLayout) findViewById(R.id.frame);

        // inicializar viewpager
        initViewPager();

        bottomNavigation = (AHBottomNavigation) findViewById(R.id.bottom_navigation);
        bottomNavigation.setDefaultBackgroundColor(Color.WHITE);
        bottomNavigation.setAccentColor(fetchColor(R.color.colorAccent));
        bottomNavigation.setInactiveColor(fetchColor(R.color.colorPrimaryDark));

        // Todo items para el menu button
        AHBottomNavigationItem item1 = new AHBottomNavigationItem("Opción 1", R.drawable.ic_printer);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem("Opción 2", R.drawable.ic_clientes);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem("Opción 3", R.drawable.ic_pedidos);
        AHBottomNavigationItem item4 = new AHBottomNavigationItem("Opción 4", R.drawable.ic_indicadores);
        AHBottomNavigationItem item5 = new AHBottomNavigationItem("Opción 5", R.drawable.ic_salir);

        bottomNavigation.addItem(item1);
        bottomNavigation.addItem(item2);
        bottomNavigation.addItem(item3);
        bottomNavigation.addItem(item4);
        bottomNavigation.addItem(item5);

        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                //fragment.setBackgroundColor(getResources().getColor(colors[position]));
                //TODO cambiar de la posición del fragmento según la posición del item botomNavigation
                if (!wasSelected)
                    viewpager.setCurrentItem(position);

                return true;
            }
        });

        bottomNavigation.setOnNavigationPositionListener(new AHBottomNavigation.OnNavigationPositionListener() {
            @Override public void onPositionChange(int y) {
                // Manage the new y position
            }
        });

        // TODO seleccionar el item por defecto
        bottomNavigation.setCurrentItem(0);
        bottomNavigation.setBehaviorTranslationEnabled(true);//se oculta cuando haces scroll

        // TODO Agregar notificaciones a los items del menu, con ciruclo personalizado
        AHNotification notification = new AHNotification.Builder()
                .setText("10")
                .setBackgroundColor(Color.YELLOW)
                .setTextColor(Color.BLACK)
                .build();
        // Adding notification to last item.
        bottomNavigation.setNotification(notification, bottomNavigation.getItemsCount() - 1);//add ultima posición
        // Add or remove notification for each item, con ciruclo de notificación por defecto
        bottomNavigation.setNotification("2", 2);

        // Enable / disable item & set disable color
        bottomNavigation.enableItemAtPosition(1);
        bottomNavigation.disableItemAtPosition(4);
        bottomNavigation.setItemDisableColor(Color.parseColor("#3A000000"));

        // Manage titles
        //bottomNavigation.setTitleState(AHBottomNavigation.TitleState.SHOW_WHEN_ACTIVE);
        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);
        //bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_HIDE);
    }

    private void initViewPager(){
        //TODO creamos el adapter
        BottomBarAdapter pagerAdapter = new BottomBarAdapter(getSupportFragmentManager());
        //TODO creamos los fragmentos
        DummyFragment fragment = new DummyFragment().newInstance(colors[0]);
        DummyFragment fragment2 = new DummyFragment().newInstance(colors[1]);
        DummyFragment fragment3 = new DummyFragment().newInstance(colors[2]);
        DummyFragment fragment4 = new DummyFragment().newInstance(colors[3]);
        DummyFragment fragment5 = new DummyFragment().newInstance(colors[4]);
        //TODO agregamos al adapter
        pagerAdapter.addFragments(fragment);
        pagerAdapter.addFragments(fragment2);
        pagerAdapter.addFragments(fragment3);
        pagerAdapter.addFragments(fragment4);
        pagerAdapter.addFragments(fragment5);

        //TODO seteamos el viewpager
        viewpager.setPagingEnabled(false);
        viewpager.setAdapter(pagerAdapter);
    }

    private int fetchColor(@ColorRes int color) {
        return ContextCompat.getColor(this, color);
    }

}
