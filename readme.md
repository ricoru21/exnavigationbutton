Custome NavigationButton
-
 ![HomePrincipal](img/screnshot2.png )

Aplicación de ejemplo donde se integra la librería 'ahbottomnavigation' como alternativa de uso al navigationButtonSupport que provee el mismo android studio.

<pre>
dependencies {
   compile 'com.aurelhubert:ahbottomnavigation:2.1.0'
}
</pre>


[Proyecto de Referencia: Github ahbottomnavigation](https://github.com/aurelhubert/ahbottomnavigation)